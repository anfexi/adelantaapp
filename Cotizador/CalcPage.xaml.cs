﻿using System;
using Cotizador.Models;
using Xamarin.Forms;
using System.Globalization;

namespace Cotizador
{
    public partial class CalcPage : ContentPage
    {

        public CalcPage()
        {
            NavigationPage.SetHasNavigationBar(this , false);
            InitializeComponent();
            Init();
            EntryVentas.Text = "";
            EntryVentas.Placeholder = $"{MailVentas.ToString("C",CultureInfo.CurrentCulture)}";
            EntryAnticipo.Text = "";
            EntryAnticipo.Placeholder = $"{MailAnticipo.ToString("C",CultureInfo.CurrentCulture)}";
           
            var Anticipo = "";
            var Ventas = "";
            var Co = "";

            EntryVentas.Unfocused += (s, e) => CHEK(EntryVentas.Text);
            EntryVentas.Unfocused += (s, e) => Ventas = EntryVentas.Text;
            EntryVentas.Unfocused += (s, e) => MailVentas = Convert.ToDouble(Ventas);
            EntryVentas.Unfocused += (s, e) => EntryVentas.Text = "";
            EntryVentas.Unfocused += (s, e) => EntryVentas.Placeholder = $"{MailVentas.ToString("C", CultureInfo.CurrentCulture)}";

            EntryAnticipo.Unfocused += (s, e) => CHEKA(EntryAnticipo.Text);
            EntryAnticipo.Unfocused += (s, e) => Anticipo = EntryAnticipo.Text;
            

            EntryAnticipo.Unfocused += (s, e) => MailAnticipo = Convert.ToDouble(Anticipo);
            EntryAnticipo.Unfocused += (s, e) => EntryAnticipo.Text = "";
            EntryAnticipo.Unfocused += (s, e) => EntryAnticipo.Placeholder = $"{MailAnticipo.ToString("C", CultureInfo.CurrentCulture)}";
            EntryAnticipo.Unfocused += (s, e) => ENTRYCheck();
            Co = EntryCompany.Text;
            MailEmpresa = Convert.ToString(Co);
            EntryCompany.Text = $"{MailEmpresa}";
        }
        void Init()
        {
            if(Device.RuntimePlatform == Device.iOS)
            {
                EntryCompany.BackgroundColor = Constants.EntGrey;
                EntryVentas.BackgroundColor = Constants.EntGrey;
                EntryAnticipo.BackgroundColor = Constants.EntGrey;
                EntryCompany.TextColor = Color.White;
                EntryVentas.TextColor = Color.White;
                EntryAnticipo.TextColor = Color.White;
                

            }
            else if(Device.RuntimePlatform == Device.Android)
            {
                EntryCompany.BackgroundColor = Color.Transparent;
                EntryVentas.BackgroundColor = Color.Transparent;
                EntryAnticipo.BackgroundColor = Color.Transparent;
                EntryCompany.TextColor = Color.Black;
                EntryVentas.TextColor = Color.Black;
                EntryAnticipo.TextColor = Color.Black;

            }
            LogoAzul.Source = ImageSource.FromFile("LogoAnticipa.png");
            LogoAzul.WidthRequest = Constants.logoWidth;
            LogoAzul.HeightRequest = Constants.logoHeight;
            

           

            LblCompany.TextColor = Color.Black;
            

            LblVentas1.TextColor = Color.Black;

            LblAnticipo1.TextColor = Color.Black;

            CalcBtn.BackgroundColor = Constants.AnticipaGrn;

            EntryCompany.Completed += (s, e) => EntryVentas.Focus();
            EntryVentas.Completed += (s, e) => EntryAnticipo.Focus();
            EntryVentas.Focused += (s, e) => EntryVentas.Placeholder = "";
            EntryAnticipo.Focused += (s, e) => EntryAnticipo.Placeholder = "";
        }
        double MailVentas,MailAnticipo,SendVent,SendAnt;
        string MailEmpresa, SendEmp;
        void CalcClicked(object sender, EventArgs e)
        {
            double VentasNetas,Anticipo;
            String company;
           

            var Comp = EntryCompany.Text;
            var Netas = MailVentas;
            var Ant = MailAnticipo;
            if (Comp == "" || MailAnticipo <=0 || MailVentas <= 0)
            {
                DisplayAlert("Error", "Uno o mas campos contienen un error o estan vacios", "ok");
            }
            else
            {
                company = Convert.ToString(Comp);
                VentasNetas = Convert.ToDouble(Netas);
                Anticipo = Convert.ToDouble(Ant);
                
                    //EntryVentas.Text = "";
                    //EntryVentas.Placeholder = $"{VentasNetas.ToString("C", CultureInfo.CurrentCulture)}";
                    //EntryAnticipo.Text = "";
                    //EntryAnticipo.Placeholder = $"{Anticipo.ToString("C", CultureInfo.CurrentCulture)}";

                    SendAnt = Anticipo;
                    SendVent = VentasNetas;
                    SendEmp = company;
                    NewPage();
                
            }
        }
        async void NewPage()
        {
            //App.Current.MainPage = new NavigationPage();
            await Navigation.PushAsync(new ResultsPage(SendVent, SendAnt, SendEmp));
        }
        void CHEKA(string A)
        {
            var C = "";
            if (A == C)
            {
                EntryAnticipo.Text = "0";
            }

        }
        void CHEK(string A)
        {
            var S = "";
            if (A == S)
            {
                EntryVentas.Text = "0";
            }
        }
        void ENTRYCheck()
        {
            double Anticipo, ventas;

            Anticipo = MailAnticipo;
            ventas = MailVentas;

            if (Anticipo > (ventas * 1.5))
            {
                CalcBtn.IsEnabled = false;
                ErrorLBL.IsVisible = true;
            }
            else if(Anticipo <= (ventas * 1.5))
            {
                CalcBtn.IsEnabled = true;
                ErrorLBL.IsVisible = false;
            }

        }
    }
}
