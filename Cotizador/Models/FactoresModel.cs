﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cotizador.Models
{
    public class FactoresModel
    {
        public int idCotizadorParametrico { get; set; }
        public double split { get; set; }
        public double fatorPago { get; set; }
        public double comision { get; set; }
        public double pMin { get; set; }
        public double pMax { get; set; }
    }
}
