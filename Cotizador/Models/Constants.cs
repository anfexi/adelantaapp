﻿using System;
using Xamarin.Forms;
namespace Cotizador.Models
{
    public class Constants
    {
        public static Color AnticipaBlu = Color.FromRgb(0, 42, 109);
        public static Color AnticipaGrn = Color.FromRgb(84, 171, 47);
        public static Color EntGrey = Color.FromRgb(183, 180, 180);
        public static string url = "http://testing.anticipa.com.mx/RegistroApi/api/";
        public static int logoHeight = 106;
        public static int logoWidth = 231;
        //public static string AntURL = "http://testing.anticipa.com.mx/RegistroApi/api/validator/ValidaCodigoPromocion";
        //public static string AntURL = "http://testing.anticipa.com.mx/RegistroApi/api/app/ValidaCodigo";
        public static string AntURL = "https://www.anticipa.com.mx:451/RegistroApi/api/app/ValidaCodigo";
        //public static string FacURL = "http://testing.anticipa.com.mx/RegistroApi/api/catalogue/cotizadorFactor";
        public static string FacURL = "https://www.anticipa.com.mx:451/RegistroApi/api/catalogue/cotizadorFactor";
        //public static string BitURL = "http://testing.anticipa.com.mx/RegistroApi/api/app/SaveBitacoraOffer";
        public static string BitURL = "https://www.anticipa.com.mx:451/RegistroApi/api/app/SaveBitacoraOffer";
        //public static string RegURL = "http://testing.anticipa.com.mx/RegistroApi/api/app/SaveBitacoraAcceso";
        public static string RegURL = "https://www.anticipa.com.mx:451/RegistroApi/api/app/SaveBitacoraAcceso";
    }
}
