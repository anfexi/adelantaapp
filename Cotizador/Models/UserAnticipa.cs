﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cotizador.Models
{
    public class UserAnticipa
    {
        public int idVendedor { get; set; }
        public string nombre { get; set; }
        public string promotoria { get; set; }
    }
}
