﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Cotizador.Models;
using Cotizador.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Xamarin.Essentials;
using Xamarin.Forms;


namespace Cotizador
{
    public partial class MainPage : ContentPage
    {
        public LoginService LoginService = new LoginService();
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            Init();
        }
        string SendCode;
        void Init()
        {
            Logo1.Source = ImageSource.FromFile("anticipa_blanco.png");
            Logo1.WidthRequest = Constants.logoWidth;
            Logo1.HeightRequest = Constants.logoHeight;
            BackgroundColor = Constants.AnticipaBlu;//color del fondo
            LblCodigo.TextColor = Color.White; //color del texto de LblCodigo
            EntryCodigo.TextColor = Color.White; //color del texto de entrada
            BtnLogIn.BackgroundColor = Constants.AnticipaGrn;
            BtnLogIn.TextColor = Color.White;
            var version = VersionTracking.CurrentVersion;
            LblVers.Text = $"Versión: {version}";
            if (Device.RuntimePlatform == Device.iOS)
            {
                EntryCodigo.BackgroundColor = Constants.EntGrey; //Color de la caja de entrada en Ios
            }
            else if(Device.RuntimePlatform == Device.Android)
            {
                EntryCodigo.BackgroundColor = Color.Transparent; //Color de la caja de entrada en android

            }
        }
        async void LogInclicked(object sender, EventArgs e)
        {
            SendCode = EntryCodigo.Text;
            var respuesta = LoginService.Login(SendCode);
            
            if (respuesta)
            {
                if (Device.RuntimePlatform == Device.iOS)
                {
                    //await DisplayAlert("Log In", "Log in Correcto", "OK");
                    App.Current.MainPage = new NavigationPage(new CalcPage());
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    App.Current.MainPage = new NavigationPage(new CalcPage());
                }
            }
            else
            {
                var app = (App)Application.Current;
                if (app.Error == "")
                {
                    await DisplayAlert("Código incorrecto", "Favor de intentar de nuevo", "OK");
                }
                else
                {
                    await DisplayAlert("Servicio no Disponible", app.Error, "OK");
                }
            }
        }
    }
}
