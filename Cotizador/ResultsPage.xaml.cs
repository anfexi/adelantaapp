﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Xamarin.Forms;
using Cotizador.Models;
using Xamarin.Essentials;
using Xamarin.Forms.Xaml;
using Cotizador.Services;

namespace Cotizador
{
    public partial class ResultsPage : ContentPage
    {
        double VentasNetas, Anticipo, Comision, IVAComision, Desembolso, AbonoEsperado, PorcAbono, Mes, fv, CostoServ, split, factorRepago, comisionAnticipo, pagoMinimo;
        int idCotParametrico, idVendedor;

        public FactoresService FactoresService = new FactoresService();
        public BitacoraService Bitacora = new BitacoraService();
        public ResultsPage(double a, double b, string c)
        {
            InitializeComponent();            
            Init();
            GetParametros();
            Dothis(a, b, c);
            var height = DeviceDisplay.MainDisplayInfo.Height;
            var width = DeviceDisplay.MainDisplayInfo.Width;
            
        }

        private void GetParametros()
        {
            var model = FactoresService.GetFactores();
            comisionAnticipo = model.comision;
            split = model.split;
            factorRepago = model.fatorPago;
            pagoMinimo = model.pMin;
            idCotParametrico = model.idCotizadorParametrico;
        }

        void Init()
        {
            LogoAzul2.Source = ImageSource.FromFile("LogoAnticipa.png");
            var height = DeviceDisplay.MainDisplayInfo.Height;
            var width = DeviceDisplay.MainDisplayInfo.Width;
            LogoAzul2.WidthRequest = Constants.logoWidth;
            LogoAzul2.HeightRequest = Constants.logoHeight;

            if (Device.RuntimePlatform == Device.iOS)
            {
                StackList.HeightRequest = 300;
            }
            else if (Device.RuntimePlatform == Device.Android)
            {                
                if (height >= 1000 && width >= 700 && DeviceDisplay.MainDisplayInfo.Density != 2)
                {
                    StackList.HeightRequest = 160;
                }
                else if((height<1000 && width < 700) || DeviceDisplay.MainDisplayInfo.Density==2)
                {
                    StackList.HeightRequest = 200;
                    
                    Lbl3.FontSize = 12;
                }
            }
            
        }
        double MailVentas, MailAnticipo, MailCom, MailIVA, MailDes, MailAbono, MailPor, MailMes;
        string MailEmpresa;

        void Dothis(double z, double x, string c)
        {
            
            String company;
            company = c;
            VentasNetas = z;
            Anticipo = x;

            fv = Anticipo * (1 + (factorRepago * 1));
            Comision = (((fv - Anticipo) / 1.16) + Anticipo) * comisionAnticipo;
            IVAComision = Comision * .16;
            Desembolso = (Anticipo - Comision);
            AbonoEsperado = (VentasNetas * split) / 30;            
            PorcAbono = split * (((fv - Anticipo) / 1.16) / fv);
            PorcAbono *= 100;
            PorcAbono = Math.Round(PorcAbono, 2);
            CostoServ = .1938 * AbonoEsperado;
            Mes = (fv / (VentasNetas * split));
            Mes = Math.Round(Mes, 1);
            var cobranzaMonto = fv - Anticipo;
            var porcDevolucion = fv / Desembolso;
            var devolucionMensual = AbonoEsperado * porcDevolucion;

            var app = (App)Application.Current;
            var user = app.UsuarioActual;

            Bitacora.SaveBitacora(user.idVendedor,VentasNetas,.5,factorRepago,fv,Comision,Desembolso,split,Mes,cobranzaMonto,comisionAnticipo,idCotParametrico, AbonoEsperado, devolucionMensual, CostoServ,company);

            MailAbono = AbonoEsperado;
            MailCom = Comision;
            MailIVA = IVAComision;
            MailDes = Desembolso;
            MailPor = PorcAbono;
            MailMes = Mes;
            MailAnticipo = Anticipo;
            MailVentas = VentasNetas;
            MailEmpresa = company;
            var Lista = new List<Items> {
                new Items {Title = "Ventas Netas Con Tarjetas Bancarias:",Text = $"{VentasNetas.ToString("C",CultureInfo.CurrentCulture)}" },
                new Items {Title = "Anticipo:",Text = $"{Anticipo.ToString("C",CultureInfo.CurrentCulture)}" },
                new Items {Title = "Comisión por Admón y Fondeo:",Text =$"{Comision.ToString("C",CultureInfo.CurrentCulture)}"},
                new Items {Title = "Iva de Comisión por Admón y Fondeo:",Text =$"{IVAComision.ToString("C",CultureInfo.CurrentCulture)}" },
                new Items {Title = "Desembolso Anticipo Neto:",Text =$"{Desembolso.ToString("C",CultureInfo.CurrentCulture)}" },
            };
            CollectionList.Header = $"{MailEmpresa}";

            CollectionList.ItemsSource = Lista;
            CollectionList.HasUnevenRows = true;

            Sp1.Text = "Abono diario esperado de: ";
            Sp2.Text = $"{AbonoEsperado.ToString("C", CultureInfo.CurrentCulture)}";
            Sp2.TextColor = Constants.AnticipaGrn;
            Sp3.Text = ", correspondiente al 15% de las ventas con tarjetas bancarias.\n";
            Sp4.Text = "\nEl costo del servicio es de ";
            Sp5.TextColor = Constants.AnticipaGrn;
            Sp5.Text = $"{CostoServ.ToString("C", CultureInfo.CurrentCulture)}";
            Sp9.Text = " pesos incluidos en cada abono diario y equivalente a 2.9% de las ventas diarias con tarjetas.";
            Sp6.Text = "\n\nTiempo esperado de Devolución: ";
            Sp7.Text = $"{Mes.ToString()}";
            Sp7.TextColor = Constants.AnticipaGrn;
            
            sp8.Text = " Meses";
        }
        //async void ShareClicked(object sender, EventArgs e)
        //{
        //    await Share.RequestAsync(new ShareTextRequest
        //    {
        //        Text = $"{MailEmpresa}\r\n\r\nVentas Netas: {MailVentas.ToString("C", CultureInfo.CurrentCulture)}\r\nAnticipo: {MailAnticipo.ToString("C", CultureInfo.CurrentCulture)}\r\n \r\nComisión por Admón y Fondeo: {MailCom.ToString("C", CultureInfo.CurrentCulture)}\r\n\r\nIva de Comisión por Admón y fondeo: {MailIVA.ToString("C", CultureInfo.CurrentCulture)}\r\n\r\nDesembolso Anticipo Neto: {MailDes.ToString("C", CultureInfo.CurrentCulture)}\r\n\r\nAbono esperado: {MailAbono.ToString("C", CultureInfo.CurrentCulture)} Que equivale a {MailPor}% de las ventas\r\n\r\nTiempo estimado:{MailMes} meses",
        //        Title = "Share"
        //    });
        //}
        
        void Selected(object sender, EventArgs e)
        {
            CollectionList.SelectedItem = null;
        }
        
    }
    public class Items
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
    
}
