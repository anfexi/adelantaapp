﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cotizador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultView : ViewCell
    {
        public ResultView()
        {
            InitializeComponent();
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
            var alt = mainDisplayInfo.Height;
            var anch = mainDisplayInfo.Width;
            if (alt >= 1000 && anch >= 700)
            {
                Lbl1.FontSize = 14;
                Lbl2.FontSize = 14;
            }
            else if(alt<1000 && anch < 700)
            {
                Lbl1.FontSize = 12;
                Lbl2.FontSize = 12;
            }
        }
    }
}
