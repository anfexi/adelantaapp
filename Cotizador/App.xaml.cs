﻿using System;
using Cotizador.Models;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cotizador
{
    public partial class App : Application
    {
        public UserAnticipa UsuarioActual { get; set; }
        public string Error { get; set; }
        public App()
        {
            InitializeComponent();
            UsuarioActual = new UserAnticipa();
            Error = "";
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            Microsoft.AppCenter.AppCenter.Start("ios=64cf7add-cf19-470f-8218-59b3e7c03aff;",typeof(Analytics), typeof(Crashes));
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
