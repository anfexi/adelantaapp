﻿using System;
using System.Net;
using Cotizador.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Xamarin.Forms;

namespace Cotizador.Services
{
    public class BitacoraService
    {

        public bool SaveBitacora(int idvendedor,
                                double ventasNetas,
                                double v,
                                double factorRepago,
                                double fv,
                                double comision,
                                double desembolso,
                                double split,
                                double mes,
                                double cobranzaMonto,
                                double comisionAnticipo,
                                int idcotParametrico,
                                double pagoMinimoMensual,
                                double devolucionMensual,
                                double comisionMensual,
                                string commerceName
            )
        {            
            

            var client = new RestClient(Constants.BitURL);
            var request = new RestRequest();

            var ivaComision = comisionMensual * .16;
            var body = new
            {
                IdVendedor = idvendedor,
                monthlySales = 0,
                monthlySalesCard = ventasNetas,
                montoDesembolsar = desembolso,
                montoPagar=fv,
                factorRepago,
                split,
                comision = comisionAnticipo,
                meses = mes,
                comisionMonto = comision,
                ratio = 0,
                desembolsoMonto = desembolso,
                devolucionMonto = desembolso,
                cobranzaMonto,
                idCotizadorParametrico = idcotParametrico,
                vecesVenta=0,
                total = fv,
                pagoMinimoMensual,
                DevolucionAnticipoMensual = devolucionMensual,
                ComisionGestionMensual = comisionMensual,
                IvaComisionGestionMensual = ivaComision,
                commerceName
            };

            request.Method = Method.POST;
            request.AddHeader("Content-Type", "application/json");
            //request.Parameters.Clear();
            request.AddParameter("", JsonConvert.SerializeObject(body), ParameterType.RequestBody);


            bool respuesta = false;

            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NoContent)
                {

                    var objeto = (JObject)JsonConvert.DeserializeObject(response.Content.ToString());
                    bool resp = objeto["valid"].Value<bool>();
                    respuesta = resp;
                }
            }
            catch (Exception error)
            {
                var i = error;
            }

            return respuesta;
        }
    }
}
