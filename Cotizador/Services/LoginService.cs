﻿using Cotizador.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Cotizador.Services
{
    public class LoginService
    {
        public bool Login(string sendCode)
        {
            var client = new RestClient(Constants.AntURL);
            var request = new RestRequest(Constants.AntURL, Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("Code", sendCode);

            bool respuesta = false;

            var app = (App)Application.Current;
            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NoContent)
                {

                    var objeto = (JObject)JsonConvert.DeserializeObject(response.Content.ToString());
                    bool resp = objeto["valid"].Value<bool>();
                    var user = JsonConvert.DeserializeObject<UserAnticipa>(objeto["object"].ToString());
                    app.UsuarioActual = user;
                    Registrar(user.idVendedor);
                    respuesta = resp;
                }
                if(response.StatusCode == HttpStatusCode.InternalServerError) {
                    app.Error = "El servicio no se encuentra disponible";
                }
            }
            catch (Exception error)
            {
                app.Error = "El servicio no se encuentra disponible";
            }

            return respuesta;
        }

        public void Registrar(int id) {
            var client = new RestClient(Constants.RegURL);
            var request = new RestRequest(Constants.RegURL, Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("IdVendedor", id);

            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NoContent)
                {

                    var objeto = (JObject)JsonConvert.DeserializeObject(response.Content.ToString());
                    bool resp = objeto["valid"].Value<bool>();
                }
            }
            catch (Exception error)
            {
                var i = error;
            }
        }
    }
}
