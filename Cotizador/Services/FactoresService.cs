﻿using Cotizador.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Xamarin.Forms;

namespace Cotizador.Services
{
    public class FactoresService
    {
        public FactoresModel GetFactores()
        {
            var model = new FactoresModel();
            var client = new RestClient(Constants.FacURL);
            var request = new RestRequest(Constants.FacURL, Method.GET);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");                        

            try
            {
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NoContent)
                {

                    var objeto = (JObject)JsonConvert.DeserializeObject(response.Content.ToString());
                    var resp = JsonConvert.DeserializeObject<FactoresModel>(objeto["object"].ToString());
                    if (resp != null)
                    {
                        model = resp;
                    }                        
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error);
            }

            return model;
        }        
    }
}
